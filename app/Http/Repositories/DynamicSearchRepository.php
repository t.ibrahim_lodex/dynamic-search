<?php

namespace App\Repositories;

use Symfony\Component\HttpFoundation\Request;

class DynamicSearchRepository
{
    public function search(Request $request)
    {
        $operation = [
            'is_equal' => '$eq',
            'not_equal' => '$ne',
            'greater_than' => '$gt',
            'less_than' => '$lt',
            'greater_than_or_equal' => '$gte',
            'less_than_or_equal' => '$lte',
        ];

        $allData =[
                [
                    "column"=>"first_name",
                    "operation"=>"is_equal",
                    "value"=>["reda"],
                    "sub_operation"=>"and",
                    "id"=>"1554280535528"
                ],
                [
                    "column"=>"gender",
                    "operation"=>"not_equal",
                    "value"=>["male"],
                    "sub_operation"=>"or",
                    "id"=>"1554280535528"
                ],
                [
                    "column"=>"last_name",
                    "operation"=>"is_equal",
                    "value"=>["ali"],
                    "sub_operation"=>"and",
                    "id"=>"1554280535528"
                ],
                [
                    "column"=>"gender",
                    "operation"=>"is_equal",
                    "value"=>["female"],
                    "sub_operation"=>null,
                    "id"=>"1554280535528"
                ]                                   
            ];
        
        $query =[];
        $and=[];
        $or = false ;

        for($i = 0; $i< count($allData); $i++)
        {
            $and []= 
                [
                    $allData[$i]['column'] =>
                    [
                        $operation[$allData[$i]['operation']] => $allData[$i]['value'][0]
                    ],
                ];
            if($allData[$i]['sub_operation'] == 'or'){
                $or = true ;
                $query []= ['$and' => $and];
                $and = [];
            }
            
            if($allData[$i]['sub_operation'] == null){
                $query []= ['$and' => $and];
                $and = [];
            }
        }
        return $query ;
    
    }
}
