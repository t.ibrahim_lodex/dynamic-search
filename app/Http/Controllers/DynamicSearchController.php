<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\DynamicSearchRepository ;

class DynamicSearchController extends Controller
{

    public $dynamicSearchRepository;

    public function __construct(DynamicSearchRepository $dynamicSearchRepository)
    {
        $this->dynamicSearchRepository = $dynamicSearchRepository;
    }

    public function index(Request $request)
    {
        $query = $this->dynamicSearchRepository->search(request()); 
       return response()->json(['$or'=> $query]);
    }
}
