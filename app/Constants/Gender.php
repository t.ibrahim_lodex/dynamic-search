<?php

namespace App\Constants;

final class Column
{
    const FIRST_NAME       = 'first_name' ;
    const LAST_NAME       = 'last_name' ;
    const GENDER       = 'gender' ;

    public static function getList()
    {
        return [
            Column::FIRST_NAME    => "first_name",
            Column::LAST_NAME => "last_name",
            Column::GENDER => "last_name",
        ];
    }

    // public static function getOne($index = '')
    // {
    //     $list = self::getList();
    //     $listOne = '';
    //     if ($index) {
    //         $listOne = $list[$index];
    //     }
    //     return $listOne;
    // }
}
