<link rel="stylesheet" href="{{ asset('css/app.css')}}">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

<div class="container" id="container">
    <br/><br/>
    <div id="filterRow" style="height:auto;">
        <div class="row" id="row" >
            <div class="col-md-2 form-group">
                 <select name="column" class="form-control column" id="" required>
                     <option value="">Select...</option>
                     <option value="first_name">first_name</option>
                     <option value="last_name">last_name</option>
                     <option value="gender">gender</option>
                 </select> 
           </div>
           <div class="col-md-2 form-group">
             <select name="operation[]" class="form-control operation" required>
                 <option value="">Select..</option>
             <option value="=">Is equal</option>
             <option value="!=">Not equal</option>
             </select> 
           </div>
           <div class="col-md-3 form-group">
             <input type="text" name="value" class="form-control value" placeholder="enter value please" id="" required>
           </div>
           <div class="col-md-2 form-group">
            <select name="condition" class="form-control condition" id="" required>
                <option value="">Select..</option>
            <option value="and">and</option>
            <option value="or">or</option>
            </select> 
          </div>       
          <div class="col-md-1 form-group">
            <input type="button" class="btn btn-info form-control search " onclick="search();"  value="search"  required>
          </div>
           <div class="col-md-1 form-group">
             <input type="submit"  class="btn btn-primary form-control newRow" value="new"  required>
           </div>
           <div class="col-md-1 form-group">
            <input type="submit" class="btn btn-danger form-control deleteRow" name="deleteRow" value="delete"  required>
          </div>
           
        </div>
        
    </div>
   
    <br />
      <h1>Count is :
         @include('count')
      </h1> 
    <table class="table table-striped">
      <thead>
      <tr>
          <th>ID</th>
          <th>first_name</th>
          <th>last_name</th>
          <th>gender</th>
      </tr>
      </thead>
      <tbody id="tbody">
        @include('table')
    </tbody>
</table>

<button id="btn_test" data-attribute = "111" class="btn btn-danger">click</button>
  </div>

<script>
    $(document).ready(function(){
     alert($(this).attr('data'));
      $("#btn_test").click(function(){
      $.get("sever", function ($(this).attr('data')) {
        console.log(data);
        }, "json");
      });


      $('.newRow').click( function(){
        var firstRow = $('#row') ;
      var number = Math.ralertound(Math.exp(Math.random()*Math.log(10000000-0+1)))+0;
      var $div = $("<div>", {id: "row-"+number , "class": "row"});
      var $div = $($div).append(firstRow.html());
      $('#filterRow').append($div);
      
      });

      // $(".deleteRow").click(function($(this)){
      //       // $(".row .form-group").find('input[name="deleteRow"]').each(function(){
      //               $(this).parent().remove();
                
      //       // });
      //   });

    });

   
    function search()
      {
        var column  = document.getElementsByClassName('column');
        var operation  = document.getElementsByClassName('operation');
        var value  = document.getElementsByClassName('value');
        var condition  = document.getElementsByClassName('condition');
        var data = [] ;
        for(var i = 0 ; i < value.length ; i++)
        {
          var request = [];
          request.unshift( column[i].value );
          request.unshift( operation[i].value);
          request.unshift( value[i].value);
          request.unshift( condition[i].value);
            data.push(request);
        }
 
         $.ajax({
          url: '{{ route("api.users.index") }}',
            type: 'get',
            data: {_token: '{{ csrf_token() }}', 'data': data},

            success: function (html) {
              $('#tbody').html(html.html);
              $('#count').html(html.count);
            },
            error: function () {
                alert("error");
            }
        });//end ajax

        

    }
    
 
  </script>